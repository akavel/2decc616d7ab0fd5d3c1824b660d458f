#!/bin/bash
set -e
set -x

PREV="$( (docker inspect -f '{{.Config.Image}}' gerrit) | sed 's/.*://' )"
case "$PREV." in
    .)      GERRIT_VERSION=2.10.x;;
    2.10.*) GERRIT_VERSION=2.11.10;;
    # # TODO(mateuszc): try going straight to 2.13.x
    2.11.*) GERRIT_VERSION=2.12.7;;
    2.12.*) GERRIT_VERSION=2.13.11;;
    2.13.*) GERRIT_VERSION=2.14.8;;
    2.14.*) GERRIT_VERSION=2.15.1;;
    *)
        echo "error: Don't know how to upgrade further from version $PREV" >&2
        exit 1;;
esac

docker stop gerrit_restore || true
docker stop gerrit || true
docker-compose up -d --build gerrit_postgres

if [[ $GERRIT_VERSION. == 2.10.* ]]; then
    # Prepare volume gerrit_index
    docker volume rm gerrit_index
    docker run \
        --rm \
        -v "gerrit_index:/gerrit/index" \
        ubuntu:16.04 \
        chown 1000:1000 /gerrit/index
fi

docker rm gerrit || true
docker run \
    --name gerrit \
    -v "$PWD/git:/gerrit/git" \
    -v "$PWD/etc:/gerrit/etc" \
    -v "gerrit_index:/gerrit/index" \
    -v "$PWD/init.sh:/gerrit/init.sh:ro" \
    --network gerrit_default \
    -p 9999:8080 \
    -p 29418:29418 \
    openfrontier/gerrit:$GERRIT_VERSION \
    bash /gerrit/init.sh

