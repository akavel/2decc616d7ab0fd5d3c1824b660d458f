set -x
set -e

id

SU_GERRIT=
if [ "$(id -u)" == "0" ]; then
    if which su-exec; then
        SU_GERRIT="su-exec $GERRIT_USER"
    else
        apk add sudo
        SU_GERRIT="sudo -u $GERRIT_USER"
    fi
fi

mkdir -p "$GERRIT_SITE"
chown "$GERRIT_USER:$GERRIT_USER" "$GERRIT_SITE"
(
    set -x
    cd /gerrit
    for dir in ./*; do
        if test -d "$dir"; then
            rm -rf "$GERRIT_SITE/$dir"
            ln -s "/gerrit/$dir" "$GERRIT_SITE/$dir"
        fi
    done
)

$SU_GERRIT java -jar "$GERRIT_WAR" init --batch -d "$GERRIT_SITE"
git config -f "$GERRIT_SITE/etc/gerrit.config" container.javaHome ""
cat "$GERRIT_SITE/etc/gerrit.config"

$SU_GERRIT java -jar "$GERRIT_WAR" reindex -d "$GERRIT_SITE"

if [[ $GERRIT_VERSION. == 2.15.* ]]; then
    # $SU_GERRIT java -jar "$GERRIT_WAR" migrate-to-note-db -d "$GERRIT_SITE" || true
    :
fi

exec $SU_GERRIT "$GERRIT_SITE/bin/gerrit.sh" daemon

